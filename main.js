const {app, BrowserWindow, shell, Menu, Tray} = require('electron')
const contextMenu = require('electron-context-menu');
const path = require('path')
app.commandLine.appendSwitch('enable-tcp-fastopen')

contextMenu({
	prepend: (defaultActions, params, browserWindow, dictionarySuggestions) => [
		{
			label: 'Search Google for “{selection}”',
			// Only show it when right-clicking text
			visible: params.selectionText.trim().length > 0,
			click: () => {
				shell.openExternal(`https://google.com/search?q=${encodeURIComponent(params.selectionText)}`);
			}
		}
	]
});

let tray = null

function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1360,
    height: 765,
    icon: __dirname + '/icon.png',
    backgroundColor: '#2C2C2C',
    webPreferences: {
     contextIsolation: true,
     spellcheck: true,
     preload: path.join(app.getAppPath(), 'preload.js')
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('splash.html')
  setTimeout(function () {
    mainWindow.loadURL('https://web.snapchat.com/',{ userAgent: "Mozilla/5.0 (Snapchat-Desktop) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36'"});
  }, 3000) // Load store page after 3 secs
  mainWindow.maximize() // start maximized
  mainWindow.setMenuBarVisibility(false)
  mainWindow.setMenu(null)
  mainWindow.show();
  mainWindow.webContents.on('new-window', function(e, url) {
    e.preventDefault();
    require('electron').shell.openExternal(url);
  });

  // Create the tray and context menu
  tray = new Tray(path.join(__dirname, 'icon.png'))
  const contextMenu = Menu.buildFromTemplate([
    { label: 'Show', click: () => { mainWindow.show() } },
    { label: 'Quit', click: () => { app.exit(0) } }
  ])
  tray.setToolTip('Snapchat')
  tray.setContextMenu(contextMenu)

  // Hide the window instead of quitting the app when the user clicks the close button
  mainWindow.on('close', (event) => {
    event.preventDefault()
    mainWindow.hide()
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
